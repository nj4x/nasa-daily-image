'use strict'
const axios = require("axios")
const twilio = require("twilio");

module.exports.handler = async (event) => {
    const {
        NASA_API_KEY,
        TWILIO_ACCT,
        TWILIO_AUTH_TOKEN,
        TWILIO_PHONE_NUMBER,
        MY_PHONE_NUMBER
    } = process.env;

    // call nasa api
    const {data: imageData} = await axios.get(`https://api.nasa.gov/planetary/apod?api_key=${NASA_API_KEY}`)

    const message = `${imageData.date}'s NASA image:
    --------------------
    ${imageData.title}.
    --------------------
     ${imageData.explanation}`

    await twilio(TWILIO_ACCT, TWILIO_AUTH_TOKEN)
        .messages
        .create({
            body: message,
            mediaUrl: [imageData.url],
            from: TWILIO_PHONE_NUMBER,
            to: MY_PHONE_NUMBER
        })
        .then(message => console.log(message.sid));

    return {
        statusCode: 200,
        body: JSON.stringify(
            {
                message: 'Go Serverless v3.0! Your function executed successfully!',
                input: event,
            },
            null,
            2
        ),
    };
};
